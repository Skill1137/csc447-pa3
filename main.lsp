#|
                  ***** EXPERT.LSP *****
 
                  "Toy" expert system routines from Chapter 18 of Winston and Horn LISP text
                  (1st edition, Addison-Wesley, 1981).
 
                  This code illustrates both forward and backward chaining expert system approaches.
 
                  Usage:
                  Code runs automatically upon loading.
          (careers <student> <n>)     - determines <n> best careers/locations for <student> using backward chaining
                  (initialize)            - initializes global variables
          (mycin <hypothesis>)         - combines certainty factor of multi-verified facts
          (mycinchain <hypothesis>)    - determines certainty factor of recursively determined facts
                  (diagnose)            - runs backward chaining expert system
                  (how <fact>)            - explains how <fact> was inferred
                  (why <fact>)            - explains why <fact> was needed
 
                  Modifications (John M. Weiss, Ph.D.)
                  11/25/98 - Rewrote and reformatted much of the archaic Lisp code.
                  11/25/98 - Removed interactive queries for more info.
                  04/04/07 - More code cleanups.

          Modiffications (Alex Muchow / Brock Benson)
               - Added careers, mycin, mycinchain functions
               - Altered many of the expert system function calls

|#
;-----------------------------------------------------------------

(defvar *asked*)
(defvar *facts*)
(defvar *hypotheses*)
(defvar *careers* nil)
(defvar *rules* nil)
(defvar *rulesused*)

;Find the career posibilitys of a student and list up to n of them
(defun careers (student &optional (n 100000000))
    (cond ((= n 0) (format t "Please try again with a larger career limit. Go for at least 1 career next time!~%")
        (return-from careers (values))))
    (initialize student)
    (diagnose student)
    (cond ((= (length *careers*) 0) (format t "~%Wow! There are no job possibilities for ~A ~%Try adding more information to the database and try again!~%~%" student) 
        (return-from careers (values))))
    (mycin *hypotheses*)
    (mycinchain *hypotheses*)
    
    (setf *facts* nil)

    
    (dolist (iterator (reverse *careers*))
        (fput student (car iterator) 'value (cdr iterator) )
    )

    ;print top n jobs
    (format t "~%Wow! ~A looks like a real winner!~%Just take a look at these wonderful career possibilities:~%~%" student)
    (let ((i 0))
        (dolist (job-iterator *careers*)
            (if (>= i n) (return))
            
            ;find best choice
            (let ((maxCF 0) (curCF) (curJob))

                (dolist (checker *careers*)
                    ;initialize a few variables
                    (setf curCF (second (car (cdr checker))))    ;CF of current item

                    ;update if find new max
                    (if (< maxCF curCF) (setf maxCF curCF))
                    (if (= maxCF curCF) (setf curJob checker))
                )
                (format t "~d) ~A = ~A with CF of ~3,2f~%"
                     (+ i 1) (car curJob) (caadr curJob) (second (cadr curJob)))
                    
                (setf *careers*    (remove curJob *careers*))
            )

            (setf i (+ i 1))
        )
    )
    (values)
)
;Determine how a fact is determined
(defun how (fact)
    (format t "~%")
    (let ((temp (copy-tree *rulesused*))
        (success))

        ; any fired rules with FACT in the then-part?
        (dolist (possibility temp)


            (when (equal (first (cddr fact)) (third (car (cdar (cdaddr possibility)))))
                (setq success t)
                ; if so, list the if-parts
                (format t "~a demonstrated by " fact)
        (if (equal (first ( car (third possibility))) 'ifall) (format t "All of~%") (format t "Some of~%"))

                (dolist (possible (cdr (caaddr possibility)))
                    (format t "~a~%" possible)
                
        )        
        (format t "~%")
        
            )

        )

        ; if not, perhaps FACT was given in original knowledge base, or is not known to be true
        (cond
            (success t)
            ((recall fact) (format t "~a was given.~%" fact) t)
            (t (format t "~a is not established.~%" fact) nil)
        )
    )
    (values)
)

; why a fact was used
(defun why (fact)
    (let
        (success)

        ; any fired rules with FACT in the if-part?
        (dolist (possibility *rulesused*)
            (dolist (ifwalk (cdr (caaddr possibility)))
                (when (and (equal (third fact) (third ifwalk)) (equal (second fact) (second ifwalk)))
                    (setq success t)
                    ; if so, list the then-parts
                    (format t "~a needed to show:~%" fact)
                    (format t "~A~%" (cadar (cdaddr possibility)))
                    (dolist (possible  (cdr (cadddr possibility)))
                        (format t "~a~%" possible)
                    )
                    (return)
                )
            )
        )

        ; if not, perhaps FACT was a hypothesis to be proved, or is not known to be true
        (cond
            (success t)
            ((recall fact) (format t "~a was hypothesis.~%" fact) t)
            (t (format t "~a was not used.~%" fact) nil)
        )
    )
    (values)
)
;Combines mycin CF's of multi-verified facts
(defun mycin (hypothesis)
    (let ((templist (copy-tree *careers*)) (temp 1)(temp2 1))

        (dolist (career templist)
            (setf templist (cdr templist))
            (dolist (career2  templist)
                (cond ((equal (caadr career) (caadr career2))

                    (setf *careers* (remove (nth (- temp 1) *careers* ) *careers*))
                    (setf *careers* (remove (nth (- (+ temp temp2) 2) *careers*) *careers*))
                    (setf *careers* (push (list (car career) 
                        (list (caadr career) (- (+ (second (cadr career)) (second (cadr career2))) 
                        (*(second (cadr career)) (second (cadr career2))  )) 0.0)) *careers*))
                    (return)
                    )
                )
                (setf temp2 (+ temp2 1))
            )
            (setf temp2 1)
            (setf temp (+ temp 1))
        )
    )
)
;Chains fact's CF's into rules that use those facts
(defun mycinchain ( hypothesis)
    (let ((templist (copy-tree *careers*)) (temp 1)(temp2 1))
        
        (dolist (career templist)
            (dolist (career2  templist)
                (cond ( (equal (car career) (first (cadr career2))) 
                    (setf *careers* (remove (nth (- temp 1) *careers* ) *careers*))
                    (setf *careers* (push (list (car career) 
                        (list (caadr career) 
                            (* (second (cadr career)) (second (cadr career2))) 0.0)) *careers*))
                    (return)
                    )
                )
                (setf temp2 (+ temp2 1))
            )
            (setf temp2 1)
            (setf temp (+ temp 1))
        )
    )    
)
;initializes all the globals for a student.
(defun initialize (student)

    ;get fact list
    (let (temp (list))
        (dolist (facts (cdr (fgetframe student)))
            (setf temp (push facts temp))
        )
        (setf *facts* (reverse temp))
    )

    ; *rulesused* is for answering "how" and "why" queries
    (setq *rulesused* nil)
    
    (setf *rules* nil)
    (loadRules 'rulebase)
    ;try to prove all possible hypotheses about animal types
    (setq *hypotheses*
        '( (job) (location) )
    )
    (setf *careers* nil)
)

;begins the process of finding career possibilities for a student
(defun diagnose (student) 
    (let ((temp 0))
        (dolist (hypothesis *hypotheses*)
            (if (not (verify hypothesis))
                (format t "Cannot find a ~D for ~D, sorry!~%" hypothesis student)
            )
            (setf *hypotheses* (remove hypothesis *hypotheses*))
            (setf temp (+ temp 1))
            (diagnose student)
        )
        nil
    )
)

;verifys a hypothesis from the global list of hypotheses. It check's if it is in the then part
;of the rulebase, if it is, it tries to fire the rule.
(defun verify (hypothesis)
    (let (relevant1 relevant2)
    
        (setq relevant1 (checkthenlist hypothesis)) ; list rules with fact in then part
        (setq relevant2 relevant1)

        ; see if fact is directly deducible
        (dolist (relevant relevant1)
            (when (tryrule relevant) )
        )

        t
    )
)


; checkthenlist makes a list of all *RULES* with hypothesis in THEN part.
(defun checkthenlist (hypothesis)
    (apply #'append (mapcar
        #'(lambda (rule) (cond ((checkthen hypothesis rule) (list rule))))
        *rules*)
    )
)


; Checkthen tests to see if a hypothesis is in the THEN part of the RULE.
(defun checkthen (hypothesis rule)
    (equal (car hypothesis) (caadar (cdaddr rule)))
)


; TRYRULE attempts to fire a RULE with TESTIF and USETHEN.
(defun tryrule (rule)
    ;calls testif and usethen
    (when (and (testif rule) (usethen rule))
        (setq *rulesused* (push rule *rulesused*))
    (setq *rules* (remove rule *rules*))
        t
    )
)


; TESTIF checks to for ifsome or ifall, and the according conditions.
; using RECALL to find known facts.
(defun testif (rule)
    (let ((ifs (caaddr rule)))
        (cond   
            ((equal (car ifs) 'ifall) 
                ;loop through IF parts of RULE
                (dolist (if-part (cdr ifs))
                    (unless (recall if-part) (return-from testif nil))
                )
                t
            )
        
            ((equal (car ifs) 'ifsome)
                (dolist (if-part (cdr ifs))
                    (if (recall if-part) (return-from testif t))  
                )
                nil
            )
        )
    )
)

; RECALL returns FACT if the condition is true, nil otherwise.
(defun recall (fact)
    (dolist (walk *facts*)
        (cond ((equal (car fact) (car walk))
            (dolist (stepin (cdadr walk))
                (if (equal (second fact) '>)
                    (if (>  ( caar stepin) (caddr fact)) (return-from recall fact)))
                (if (equal (second fact) '>=)
                    (if (>= ( caar stepin) (caddr fact)) (return-from recall fact)))
                (if (equal (second fact) '<=)
                    (if (<= ( caar stepin) (caddr fact)) (return-from recall fact)))
                (if (equal (second fact) '<)
                    (if (< ( caar stepin) (caddr fact)) (return-from recall fact)))
                (if (equal (second fact) '/=)
                    (if (not (equal ( caar stepin) (caddr fact)) (return-from recall fact))))
                (if (equal (second fact) '=)
                    (if (equal ( caar stepin) (caddr fact)) (return-from recall fact)))
            ) 
        ))
    )
    nil
)

; USETHEN attempts to add new facts as a result of firing the RULE,
; by checking all of the THEN parts.
(defun usethen (rule)
    (let
        (
            (thens  (cdar (cddr rule)))
            (success t)
        )


        (cond 
            ((equal success t) 
                (setq *facts* (reverse *facts*))
                (setq *hypotheses* (append *hypotheses* (list (list (first  (cdr (cdadar thens)))) )))
                (setq *careers* (push (list (caadar thens)
                    (list (first (cdr (cdadar thens))) 
                        (first (cdadr thens)) 0.0)) *careers*))
                (setq *facts* (push (list (caadar thens) 
                    (list 'value  (list (list  (first (cdr (cdadar thens))) 
                        (first (cdadr thens)) 0.0)))) *facts*))
                (setq *facts* (reverse *facts*))
            )
        )
    )
)

;load all the rules
(defun loadRules (filename)

    (let ((counter 0) tempchar templist)
        ;Read through file using with-open-file
            (with-open-file (fin filename :if-does-not-exist nil)
                    (when (null fin) (return-from loadRules (format nil "Error: cannot open file ~a" filename)))
                            (do ((data (read fin nil) (read fin nil)))     ; read entire file, returning NIL at EOF
                                ((null data) 'done)                ; exit when file is read
                                (setf counter (+ counter 1))
                                (setf tempchar (format nil "Rule~d" counter))
                                (setf *rules* (cons (list 'rule tempchar data) *rules*))
                            )
            )
    )
    (setf *rules* (reverse *rules*))
)
(load 'frames.lsp)
(buildDatabase 'students)
