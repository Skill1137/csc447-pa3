#|
                ***** FRAMES.LSP *****

Frame-based knowledge representation routines from Chapter 22 of Winston and Horn LISP text
(1st edition, Addison-Wesley, 1981). Frames are stored as associations lists on symbol property lists.
Value facets are stored as (value mb md), where mb and md are MYCIN measures of belief and disbelief.

Usage:
Code runs automatically upon loading.
(fget frame slot facet)             - retrieve SLOT/FACET information from FRAME
(fput frame slot facet value)       - add SLOT/FACET/VALUE to FRAME
(fremove frame slot facet value)    - remove SLOT/FACET/VALUE information from FRAME
(fgetframe frame)                   - return the entire FRAME info
(buildDatabase filename)            - builds database from file

Modifications (John M. Weiss, Ph.D.)
11/25/98 - Removed interactive queries for more info.
04/04/07 - More code cleanups.

Modifications (Brock Benson / Alex Muchow)
     - Added buildDatabase function
     - Removed sample output at end of file
|#

; FGET retrieves SLOT/FACET information from the FRAME.
(defun fget (frame slot facet)
    (mapcar #'car
        (cdr (assoc facet
            (cdr (assoc slot
                (cdr (get frame 'frame))
            ))
        ))
    )
)

; FPUT adds SLOT/FACET/VALUE to the FRAME if not already present.
(defun fput (frame slot facet value)
    (cond
        ; value is list (VALUE MB MD), so need EQUAL test
        ((member value (fget frame slot facet) :test #'equal) nil)
        (t (fassoc value (fassoc facet (fassoc slot (fgetframe frame)))) value)
    )
)

; FGETFRAME is called by FPUT to return the FRAME if it already exists,
; or create the FRAME if it does not.
(defun fgetframe (frame)
    (cond
        ((get frame 'frame))
        (t (setf (get frame 'frame) (list frame)))
    )
)

; FASSOC is called by FPUT to build frames. It acts like ASSOC when KEY
; is found on A-LIST, otherwise it appends KEY to the end of A-LIST.
(defun fassoc (key a-list)
    (cond
        ((assoc key (cdr a-list)))
        (t (cadr (rplacd (last a-list) (list (list key)))))
    )
)

; FREMOVE removes SLOT/FACET/VALUE information from the FRAME.
(defun fremove (frame slot facet value)
    (let (slots facets values target)
        (setq slots (fgetframe frame))
        (setq facets (assoc slot (cdr slots)))
        (setq values (assoc facet (cdr facets)))
        ; value is list (VALUE MB MD), so need EQUAL test
        (setq target (assoc value (cdr values) :test #'equal))
        (delete target values)
        (when (null (cdr values)) (delete values facets))
        (when (null (cdr facets)) (delete facets slots))
        (not (null target))     ; return T if removed something, else NIL
    )
)

(defun buildDatabase (filename)

        ;Read through file using with-open-file
            (with-open-file (fin filename :if-does-not-exist nil)
                    (when (null fin) (return-from buildDatabase (format nil "Error: cannot open file ~a" filename)))
                                (do ((data (read fin nil) (read fin nil)))      ; read entire file, returning NIL at EOF
                                            ((null data) 'done)                 ; exit when file is read
                            (fput (first data)  (second data)'value  (list (third data) '1.0 '0.0))
                                )
            )
)
